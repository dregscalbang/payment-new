<?php 

/*
Template Name: Special Layout
*/

get_header();

if (have_posts()) :
	while (have_posts()) : the_post(); ?>

	<article class="post page">

		<h2><?php the_title(); ?></h2>

		<!-- info-box -->
		<div class="info-box">
			<h4>Disclaimer (Read Me)</h4>
			<p>The information in this blog is true and came from variety resources from the net. The authors and the developer disclaim any liability in connection with the use of this information.</p>
		</div><!-- /info-box -->

		<?php the_content(); ?>

	</article>

		<?php endwhile;

		else :
			echo '<p>No content found</p>';
		endif;

	get_footer();

	?>